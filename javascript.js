window.onload = function () {
    if (localStorage.getItem("theme") !== null) {
        let themesName = localStorage.getItem("theme");
        document.getElementById("change-theme").setAttribute("data-theme",`${themesName}`);
    } 
}
document.getElementById("btn-change-themes").addEventListener("click", function () {
    if(localStorage.getItem("theme")==="dark"){
        document.getElementById("change-theme").removeAttribute("data-theme")
        localStorage.setItem("theme","")
    }else {
    document.getElementById("change-theme").setAttribute("data-theme", "dark")
    localStorage.setItem("theme", "dark");}

})
